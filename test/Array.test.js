import { expect, test, describe } from "vitest"
// import "./index"
import "../Array"

/**
 * @author: 微若蜉蝣
 * @Blog :https://fuyouplus.cn
 * @Date :2022/7/25
 * @description: 手写数组所有方法的测试用例
 */

/**
 * @Name forEach
 */
describe("forEach", () => {
  test("回调函数执行数组的 length 次", () => {
    const arr = [1, 2, 3, 4, 5]
    let count = 0
    arr.myForEach(item => {
      expect(item).toBe(arr[count])
      count++
    })
    expect(count).toBe(arr.length)
  })
})

/**
 * @Name Filter
 */
describe("filter", () => {
  test("数组包数字", () => {
    const arr = [1, 2, 3, 4, 5]
    expect(arr.myFilter(item => item > 3)).toEqual([4, 5])
  })

  test("数组包对象", () => {
    const arr = [{ name: "a" }, { name: "b" }, { name: "c" }]
    expect(arr.myFilter(item => item.name === "a")).toEqual([{ name: "a" }])
  })

  test("返回的是一个新数组并非数组的引用", () => {
    const arr = [{ name: "a" }, { name: "b" }, { name: "c" }]
    expect(arr.myFilter(item => item.name === "a"))
      .toEqual([{ name: "a" }])
      .not.toBe(arr)
  })
})

/**
 * @Name Some
 */
describe("Some", () => {
  test("数组包数字", () => {
    const arr = [1, 2, 3, 4, 5]
    expect(arr.mySome(item => item > 3)).toBe(true)
  })

  test("数组包对象", () => {
    const arr = [{ name: "a" }, { name: "b" }, { name: "c" }]
    expect(arr.mySome(item => item.name === "ab")).toBe(false)
  })
})

/**
 * @Name Every
 */
describe("Every", () => {
  test("数组包数字", () => {
    const arr = [1, 2, 3, 4, 5]
    expect(arr.myEvery(item => item > 3)).toBe(false)
  })

  test("数组包对象", () => {
    const arr = [{ name: "a" }, { name: "b" }, { name: "c" }]
    expect(arr.myEvery(item => item.name !== "ab")).toBe(true)
  })
})

/**
 * @Name Push
 */
describe("Push", () => {
  test("push 一个返回length", () => {
    const arr = [1, 2, 3, 4]
    expect(arr.myPush(5)).toEqual(5)
  })

  test("push 多个返回length", () => {
    const arr = [1, 2]
    expect(arr.myPush(3, 4, 5)).toEqual(5)
  })

  test("push 后改变原数组,不是返回新数组", () => {
    const arr = [1, 2]
    expect(arr.myPush(3, 4, 5)).toEqual(arr.length).toBe(5)
  })
})

/**
 * @Name deepCopy
 */
describe("deepCopy", () => {
  test("copy 一维基本类型数组的所有值", () => {
    const arr = [1, 2, 3, 4]
    const copyArr = arr.myDeepCopy()
    expect(copyArr).toEqual([1, 2, 3, 4])
  })

  test("copy 一维基本类型数组的所有值,但不是引用地址", () => {
    const arr = [1, 2, 3, 4]
    const copyArr = arr.myDeepCopy()
    expect(copyArr).toEqual([1, 2, 3, 4]).not.toBe(arr)
  })
  test("copy 多维引用类型数组的所有值", () => {
    const arr = [[{ name: "a" }], [{ name: "b" }], [{ name: "c" }]]
    const copyArr = arr.myDeepCopy()
    expect(copyArr).toEqual([[{ name: "a" }], [{ name: "b" }], [{ name: "c" }]])
  })

  test("copy 多维引用类型数组的所有值,但不是引用地址", () => {
    const arr = [[{ name: "a" }], [{ name: "b" }], [{ name: "c" }]]
    const copyArr = arr.myDeepCopy()
    arr[0].name = "d"
    expect(copyArr)
      .toEqual([[{ name: "a" }], [{ name: "b" }], [{ name: "c" }]])
      .not.toBe(arr)
  })
})

/**
 * @Name Concat
 */
describe("Concat", () => {
  test("concat 一个数组", () => {
    const arr = [1, 2, 3, 4]
    expect(arr.myConcat([5, 6, 7])).toEqual([1, 2, 3, 4, 5, 6, 7])
  })
  test("concat 多个数组", () => {
    const arr = [1, 2, 3, 4]
    expect(arr.myConcat([5, 6, 7], [8, 9, 10], [11, 12, 13])).toEqual([
      1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
    ])
  })
  test("concat 后返回的是一个新数组,不会改变原数组", () => {
    const arr = [1, 2, 3, 4]
    const concatArr = arr.myConcat([5, 6, 7])
    expect(concatArr).toEqual([1, 2, 3, 4, 5, 6, 7])
    expect(arr).toEqual([1, 2, 3, 4])
    expect(arr).not.toBe(concatArr)
  })
})
/**
 * @Name indexOf
 */
describe("indexOf", () => {
  test("只传一个参数的时候|true", () => {
    const arr = [1, 2, 3, 4]
    expect(arr.myIndexOf(4)).toBe(3)
  })
  test("只传一个参数的时候|false", () => {
    const arr = [1, 2, 3, 4]
    expect(arr.myIndexOf(5)).toBe(-1)
  })
  test("只传一个参数的时候|false|开启严格比较模式 ===", () => {
    const arr = [1, 2, 3, 4]
    expect(arr.myIndexOf("4", null, true)).toBe(-1)
  })
  test("传两个参数的时候(指定startIndex)|startIndex大于数组 Length 的情况", () => {
    const arr = [1, 2, 3, 4]
    expect(arr.myIndexOf(4, arr.length + 1)).toBe(-1)
  })
  test("传两个参数的时候(指定startIndex)|startIndex大于0的情况", () => {
    const arr = [1, 2, 3, 4]
    expect(arr.myIndexOf(4, 1)).toBe(3)
  })
  test("传两个参数的时候(指定startIndex)|startIndex小于0的情况|findIndex = length + findIndex|还是小于0", () => {
    const arr = [1, 2, 3, 4]
    expect(arr.myIndexOf(4, -(arr.length + 3))).toBe(3)
  })
  test("传两个参数的时候(指定startIndex)|startIndex小于0的情况|findIndex = length + findIndex|还是小于0", () => {
    const arr = [1, 2, 3, 4]
    expect(arr.myIndexOf(5, -(arr.length + 3))).toBe(-1)
  })
  test("传两个参数的时候(指定startIndex)|startIndex小于0的情况|findIndex = length + findIndex|大于0", () => {
    const arr = [1, 2, 3, 4]
    expect(arr.myIndexOf(2, -2)).toBe(-1)
  })
  test("传两个参数的时候(指定startIndex)|startIndex小于0的情况|findIndex = length + findIndex|大于0", () => {
    const arr = [1, 2, 3, 4]
    expect(arr.myIndexOf(3, -2)).toBe(2)
  })
})
/**
 * @Name Includes
 */
describe("includes", () => {
  test("只传一个参数的时候|true", () => {
    const arr = [1, 2, 3, 4]
    expect(arr.myIncludes(4)).toBe(true)
  })
  test("只传一个参数的时候|false", () => {
    const arr = [1, 2, 3, 4]
    expect(arr.myIncludes(5)).toBe(false)
  })
  test("只传一个参数的时候|false|开启严格比较模式 ===", () => {
    const arr = [1, 2, 3, 4]
    expect(arr.myIncludes("4", null, true)).toBe(false)
  })
  test("传两个参数的时候(指定startIndex)|startIndex大于数组 Length 的情况", () => {
    const arr = [1, 2, 3, 4]
    expect(arr.myIncludes(4, arr.length + 1)).toBe(false)
  })
  test("传两个参数的时候(指定startIndex)|startIndex大于0的情况", () => {
    const arr = [1, 2, 3, 4]
    expect(arr.myIncludes(4, 1)).toBe(true)
  })
  test("传两个参数的时候(指定startIndex)|startIndex小于0的情况|findIndex = length + findIndex|还是小于0", () => {
    const arr = [1, 2, 3, 4]
    expect(arr.myIncludes(4, -(arr.length + 3))).toBe(true)
  })
  test("传两个参数的时候(指定startIndex)|startIndex小于0的情况|findIndex = length + findIndex|还是小于0", () => {
    const arr = [1, 2, 3, 4]
    expect(arr.myIncludes(5, -(arr.length + 3))).toBe(false)
  })
  test("传两个参数的时候(指定startIndex)|startIndex小于0的情况|findIndex = length + findIndex|大于0", () => {
    const arr = [1, 2, 3, 4]
    expect(arr.myIncludes(2, -2)).toBe(false)
  })
  test("传两个参数的时候(指定startIndex)|startIndex小于0的情况|findIndex = length + findIndex|大于0", () => {
    const arr = [1, 2, 3, 4]
    expect(arr.myIncludes(3, -2)).toBe(true)
  })
})
/**
 * @Name Splice
 */
describe("splice", () => {
  test()
})
