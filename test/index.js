let arr = [1, 2, 3, 4, 5]
Array.prototype.myForEach = function myForEach() {}

//test

// arr.myForEach((item, index, arr) => {
//   console.log(item)
//   console.log(index)
// })

Array.prototype.myFilter = function myFilter(func) {}

//test
// const newArr = arr.myFilter(item => item > 2)
// console.log(newArr)

Array.prototype.mySome = function mySome() {}

//test
// console.log(arr.mySome(item => item == 7))

Array.prototype.myEvery = function myEvery() {}
//test
// console.log(arr.myEvery(item => item == 7))
Array.prototype.myPush = function myPush() {}
//test
// console.log(arr.myPush(6, 7, 8))
// console.log(arr)

Array.prototype.myDeepCopy = function myDeepCopy() {}
//test
// const arr2 = [{ name: [1, 2, 3] }, { name: [4, 5, 6] }]
// const arr2 = [1, 2, 3, 4]
// const arr3 = arr2.myDeepCopy()
// arr2[0] = 99
// console.log(arr3)
// console.log(arr2.myDeepCopy())

Array.prototype.myConcat = function myConcat() {}
//test
// let arr2 = arr.myConcat([6, 7, 8], [9, 10, 11], [12, 13, 14], [15, 16, 17])
// console.log("arr2=", arr2)

Array.prototype.myIndexOf = function myIndexOf() {}
//test
// console.log(arr.myIndexOf(1))
// console.log(arr.myIndexOf("1", null, false))
// console.log(arr.myIndexOf(1, 1)) - 1
// console.log(arr.myIndexOf("1", 0, false))

Array.prototype.myIncludes = function myIncludes() {}
//test
// console.log(arr.myIncludes(1)) //true
// console.log(arr.myIncludes("1", null, false)) //true
// console.log(arr.myIncludes(1, 1)) //false
// console.log(arr.myIncludes("1", 0, false)) //true

Array.prototype.mySplice = function mySplice() {}
//test
// let res2 = arr.mySplice(0, 3)

// console.log(res2)

// console.log(res2 === arr) //false

// console.log(arr)

Array.prototype.mySlice = function mySlice() {}
//test
// let res2 = arr.mySlice(1, 3)

// console.log(res2)

// console.log(res2 === arr) //false

// console.log(arr)

Array.prototype.myReverse = function myReverse() {}
//test
// arr = [1, 2, 3, 4, 5] //奇数
// arr = [1, 2, 3, 4, 5, 6] //偶数

// let arr2 = JSON.parse(JSON.stringify(arr))
// let res = arr.myReverse()

// console.log("arr2=", arr2)
// console.log("res=", res)
// console.log(arr === res)

Array.prototype.myFill = function myFill() {}
//test
// const arr2 = new Array(5)
// const res = arr2.myFill({ name: "zz" })
// arr2[0].name = "11"
// console.log(arr2)
// console.log(res)
// console.log(res === arr2)

Array.prototype.myFind = function myFind() {}

//test
// const res = arr.myFind(item => item == 33)
// console.log(res)

Array.prototype.myFindIndex = function myFindIndex() {}

//test
// const res = arr.myFindIndex(item => item == 3)
// console.log(res)

Array.prototype.myAt = function myAt() {}
//test
// console.log(arr.myAt(2)) //3
// console.log(arr.myAt(-1)) //5

Array.prototype.myFindLast = function myFindLast() {}

//test
// const res = arr.myFindLast(item => item > 3)
// console.log(res)

Array.prototype.myFindLastIndex = function myFindLastIndex() {}

//test
// const res = arr.myFindLastIndex(item => item > 3)
// console.log(arr)
// console.log(res)

Array.prototype.myJoin = function myJoin() {}
//test
// const arr2 = ["a", "b", "c"]
// const res = arr2.myJoin("-")
// console.log(res)

Array.prototype.myKeys = function myKeys() {}
//test
// const res = arr.myKeys()
// console.log(res)

Array.prototype.myLastIndexOf = function myLastIndexOf() {}
//test
// const animals = ["Dodo", "Tiger", "Penguin", "Dodo"]
// console.log(animals.myLastIndexOf("Dodo"))
// // expected output: 3

// console.log(animals.myLastIndexOf("Tiger"))
// // expected output: 1

Array.prototype.myMap = function myMap() {}
//test
// const array1 = [1, 4, 9, 16]

// // pass a function to map
// const map1 = array1.myMap(x => x * 2)

// console.log(map1)
// // expected output: Array [2, 8, 18, 32]

Array.prototype.myEntries = function myEntries() {}
//test
// const array1 = ["a", "b", "c"]

// const iterator1 = array1.myEntries()
// console.log(iterator1.next())
// console.log(iterator1.next())
// console.log(iterator1.next())
// console.log(iterator1.next())
// console.log(iterator1.next())
// console.log("----------------")
// for (let key of iterator1) {
//   console.log(key)
// }

Array.prototype.myUnique = function myUnique() {
  //扩展函数 去重
}
//test
// const testArr = [1, 1, 2, 3, 4, 4, 4, 5, 5, 6, 6]
// console.log(testArr.myUnique())
// const testArr = [{ name: "zz" }, { name: "zz" }, { name: "aa" }, { name: "bb" }, { name: "bb" }]
// console.log(testArr.myUnique("name"))

Array.prototype.myGroup = function myGroup() {}
//test
// const inventory = [
//   { name: "asparagus", type: "vegetables", quantity: 5 },
//   { name: "bananas", type: "fruit", quantity: 0 },
//   { name: "goat", type: "meat", quantity: 23 },
//   { name: "cherries", type: "fruit", quantity: 5 },
//   { name: "fish", type: "meat", quantity: 22 },
// ]

// console.log(inventory.myGroup(({ type }) => type))

// function myCallback({ quantity }) {
//   return quantity > 5 ? "ok" : "restock"
// }
// console.log(inventory.myGroup(myCallback))

Array.prototype.myGroupToMap = function myGroupToMap() {}
//test
// const inventory = [
//   { name: "asparagus", type: "vegetables", quantity: 9 },
//   { name: "bananas", type: "fruit", quantity: 5 },
//   { name: "goat", type: "meat", quantity: 23 },
//   { name: "cherries", type: "fruit", quantity: 12 },
//   { name: "fish", type: "meat", quantity: 22 },
// ]
// const restock = { restock: true }
// const sufficient = { restock: false }
// const result = inventory.myGroupToMap(({ quantity }) => (quantity < 6 ? restock : sufficient))
// console.log(result.get(restock))
// expected output: Array [Object { name: "bananas", type: "fruit", quantity: 5 }]

Array.prototype.myPop = function myPop() {}

//test
// const plants = ["broccoli", "cauliflower", "cabbage", "kale", "tomato"]

// console.log(plants.myPop())
// // expected output: "tomato"

// console.log(plants)
// // expected output: Array ["broccoli", "cauliflower", "cabbage", "kale"]

// plants.myPop()

// console.log(plants)
// // expected output: Array ["broccoli", "cauliflower", "cabbage"]

Array.prototype.myShift = function myShift() {}

//test
// const array1 = [1, 2, 3]

// const firstElement = array1.myShift()

// console.log(array1)
// // expected output: Array [2, 3]

// console.log(firstElement)
// // expected output: 1

Array.prototype.myUnshift = function myUnshift() {}
//test
// const array1 = [1, 2, 3]

// console.log(array1.myUnshift(4, 5))
// // expected output: 5

// console.log(array1)
// // expected output: Array [4, 5, 1, 2, 3]

Array.prototype.myValues = function myValues() {}

//test
// const array1 = ["a", "b", "c"]
// const iterator = array1.myValues()
// console.log(iterator)
// for (let key of iterator) {
//   console.log(key)
// }

// // expected output: "a"
// // expected output: "b"
// // expected output: "c"
// const arr2 = ["a", "b", "c", "d", "e"]
// const iterator = arr2.myValues()
// for (let letter of iterator) {
//   console.log(letter)
// } //"a" "b" "c" "d" "e"
// for (let letter of iterator) {
//   console.log(letter)
// } //undefined

Array.prototype.myToString = function myToString() {}

//test
// const array1 = [1, 2, "a", "1a"]
// console.log(array1.toString())
// // expected output: "1,2,a,1a"

Array.prototype.mySort = function mySort() {}
//test
// const arr2 = [1, 3, 8, 5, 4, 6, 7, 2, 9]
// console.log(arr2.mySort((a, b) => b > a))
// const arr2 = [{ age: 11 }, { age: 9 }, { age: 22 }, { age: 10 }, { age: 8 }]
// console.log(arr2.mySort((a, b) => b.age < a.age))

Array.prototype.myReduce = function myReduce(...args) {}
//test
// const array1 = [1, 2, 3, 4]
// const initialValue = 0
// const sumWithInitial = array1.myReduce(
//   (previousValue, currentValue) => previousValue + currentValue,
//   initialValue
// )
// console.log(sumWithInitial) //10

// let initialValue = 0
// let sum = [{ x: 1 }, { x: 2 }, { x: 3 }]
// const res = sum.myReduce(function (previousValue, currentValue) {
//   return previousValue + currentValue.x
// }, initialValue)

// console.log(res) // logs 6

Array.prototype.myReduceRight = function myReduceRight() {}
//test
// const array1 = [
//   [0, 1],
//   [2, 3],
//   [4, 5],
// ]

// const result = array1.myReduceRight((accumulator, currentValue) => accumulator.concat(currentValue))

// console.log(result)
// // expected output: Array [4, 5, 2, 3, 0, 1]

// var sum = [0, 1, 2, 3].reduceRight(function (a, b) {
//   return a + b
// })
// console.log(sum) // sum is 6

Array.prototype.myCopyWithin = function myCopyWithin() {}
//test
// const array1 = ["a", "b", "c", "d", "e"]
// // copy to index 0 the element at index 3
// console.log(array1.myCopyWithin(0, 2, 4))
// // expected output: Array ["d", "b", "c", "d", "e"]
// // copy to index 1 all elements from index 3 to the end
// console.log(array1.myCopyWithin(1, 3))
// // expected output: Array ["d", "d", "e", "d", "e"]
Array.myIsArray = function myIsArray() {}
//test
//类数组 || 伪数组
// console.log("arr=", arr)
// let fakeArray = {
//   0: "a",
//   1: "b",
//   length: 2,
// }
// fakeArray.constructor = Array
// fakeArray.__proto__ = Array.prototype
// let fakeArray2 = Array.from(fakeArray)
// fakeArray2[0] = 22
// console.log(fakeArray)
// console.log(fakeArray2)
// console.log(fakeArray === fakeArray2)
// console.log(Array.myIsArray(fakeArray2))
// fakeArray.push("c")
// console.log("fakeArray=", fakeArray)
// console.log("pop=", fakeArray.pop())
// console.log("fakeArray2=", fakeArray)
// console.log(Array.myIsArray(fakeArray))
// // expected output: false
Array.prototype.myFlat = function myFlat() {}
//test
// const arr2 = [1, 2, 3, [4, 5, [6, [7]]], [8, [9, 10, [11, 12]]]]
// const arr3 = [1, 2, 3, [4, 5, [6, [7]]], [8, [9, 10, [11, 12]]]]
// console.log("arr2=", arr2)
// console.log(arr2.myFlat())
// console.log(arr3.flat(Infinity))

Array.prototype.myFlatMap = function myFlatMap() {}
//test
// const arr1 = [1, 2, [3], [4, 5], 6, []]

// const flattened = arr1.myFlatMap(num => num)

// console.log(flattened)
// expected output: Array [1, 2, 3, 4, 5, 6]

// const arr1 = [1, 2, 3, 4]
// console.log(arr1.myFlatMap(x => [[x * 2]]))
// [[2], [4], [6], [8]]

Array.myFrom = function myFrom() {}
//test
// console.log(Array.myFrom("foo"))
// // expected output: Array ["f", "o", "o"]

// console.log(Array.myFrom([1, 2, 3], x => x + x))
// // expected output: Array [2, 4, 6]

Array.myOf = function myOf() {}
//test
// console.log(Array.myOf(7)) // [7]
// console.log(Array.myOf(1, 2, 3)) // [1, 2, 3]
